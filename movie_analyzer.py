import numpy as np
import pandas as pd
import json
import logging


class MovieAnalyzer:
    def __init__(self):
        self.logger = logging.getLogger(__name__)

    @staticmethod
    def read_chunk(chunk_number):
        chunk_file = f'data/ratings_{chunk_number}.csv'
        return pd.read_csv(chunk_file)


    def load_dataset(self):
        """
        Load movies_metadata.csv and ratings.cvs as DataFrames.Filter movies_metadata_df to include only rows with valid
        integer IDs and fix typing of IDs.
        """
        dataframes = [self.read_chunk(i + 1) for i in range(7)]
        self.ratings_df = pd.concat(dataframes, ignore_index=True)

        self.movies_metadata_df = pd.read_csv('data/movies_metadata.csv', low_memory=False)

        numeric_ids = pd.to_numeric(self.movies_metadata_df['id'], errors='coerce')
        valid_id_mask = ~np.isnan(numeric_ids)
        self.movies_metadata_df = self.movies_metadata_df[valid_id_mask]
        self.movies_metadata_df['id'] = self.movies_metadata_df['id'].astype(int)

        self.logger.info("Data loaded successfully.")

    def unique_movies_count(self):
        """
        Removes the rows that contain duplicated IDs and returns the count of all rows.
        """
        self.movies_metadata_df = self.movies_metadata_df.drop_duplicates(subset='id', keep='first')
        return len(self.movies_metadata_df)

    def average_rating(self):
        """
        Returns the mean rating from the ratings dataset
        """

        return self.ratings_df['rating'].mean()

    def top_rated_movies(self, n=5):
        """
        Calculate the weighted average for each movieId and merge this data with the movie data. Save the result to be
        written as JSON later. Finally, return the top 5 by weighted rating.
        """

        grouped_data = self.ratings_df.groupby('movieId')
        self.ratings_df['weighted_rating'] = self.ratings_df['rating'] * grouped_data['rating'].transform('count')
        sum_weighted_ratings = grouped_data['weighted_rating'].transform('sum')
        total_weight = grouped_data['rating'].transform('count')
        self.ratings_df['weighted_average_rating'] = sum_weighted_ratings / total_weight
        weighted_ratings_df = self.ratings_df[['movieId', 'weighted_average_rating']].drop_duplicates()

        merged_df = pd.merge(self.movies_metadata_df, weighted_ratings_df, left_on='id', right_on='movieId')
        self.merged_df = merged_df

        top_movies = merged_df.nlargest(5, 'weighted_average_rating')
        return top_movies[['title', 'weighted_average_rating']]

    def movies_per_year(self):
        """
        Grabs the first 4 characters from 'release_date' column to get the appropriate year and return the value
        counts as a dictionary sorted by the year.
        """

        movies_per_year = self.movies_metadata_df['release_date'].str.slice(0, 4).value_counts().to_dict()
        return dict(sorted(movies_per_year.items(), key=lambda x: x[0], reverse=True))

    def movies_per_genre(self):
        """
        Turns the genre containing lists into JSON to enable item access into the inner dictionaries. Then, the count
         by genres dictionary is populated and returned.
        """

        genres_count = {}
        for genre_list in self.movies_metadata_df['genres']:
            genres = json.loads(genre_list.replace("'", "\""))
            for genre in genres:
                genre_name = genre['name']
                if genre_name in genres_count:
                    genres_count[genre_name] += 1
                else:
                    genres_count[genre_name] = 1
        return genres_count

    def save_to_json(self, output_file):
        try:
            self.merged_df.to_json(output_file, orient='records')
        except AttributeError:
            print("The final data has not been merged yet, please run MovieAnalyzer.top_rated_movies before saving to JSON")
        else:
            self.logger.info("Data saved to JSON file successfully.")
