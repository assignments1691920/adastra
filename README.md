The MovieAnalyzer class is used to provide an answer per question through its methods. The driver code that I used to test it is in main.py. 

The source data is chunked due to file size restrictions in uploading to GitLab. The data is loaded in chunks for thsi reason.

 In order to merge the movies_metadata and rating DataFrames I had to first cast the id column of the former as an int. In the process of this I found that movies_metadata actually contains 3 rows with invalid IDs and I decided to delete them. These rows can be corrected since the information appears to be misplaced but available. I created a dictionary that has the column name for these rows as keys and the actual data they contain as values.


    {
        'adult': 'overview'
        'belongs_to_collection': 'popularity'
        'budget:' 'poster_path'
        'genres': 'production_companies'
        'homepage': 'production_countries'
        'id': 'release_date'
        'original_language': 'runtime'
        'original_title': 'spoken_languages'
        'overview': 'status'
        'popularity': 'taglines'
        'poster_path': 'original_title'
        'production_companies': 'adult'
        'production countries': 'overal_vote'
        'release_date': id  
    }

In the end I decided to delete them nevertheless, because one the supposed IDs, which appear to have been listed under 'release_date' column, appear elsewhere in the DataFrame and point to a different movie. So I preferred to treat these rows as erroneous data and would otherwise affect the count and average rating results.