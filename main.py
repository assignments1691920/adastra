import logging

from movie_analyzer import MovieAnalyzer


def main():
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    output_json_file = 'movie_data.json'

    analyzer = MovieAnalyzer()
    analyzer.load_dataset()

    # Print the number of unique movies
    unique_movies_count = analyzer.unique_movies_count()
    print(f"Number of unique movies: {unique_movies_count}")

    # Print the average rating of all movies
    average_rating = analyzer.average_rating()
    print(f"Average rating of all movies: {average_rating:.2f}")

    # Print the top 5 highest rated movies
    top_rated_movies = analyzer.top_rated_movies()
    print("Top 5 highest rated movies:")
    print(top_rated_movies)

    # Print the number of movies released each year
    movies_per_year = analyzer.movies_per_year()
    print("Number of movies released each year:")
    for year, count in movies_per_year.items():
        print(f"{year}: {count}")

    # Print the number of movies in each genre
    movies_per_genre = analyzer.movies_per_genre()
    print("Number of movies in each genre:")
    for genre, count in movies_per_genre.items():
        print(f"{genre}: {count}")

    # Save the dataset to a JSON file
    analyzer.save_to_json(output_json_file)



if __name__ == "__main__":
    main()
